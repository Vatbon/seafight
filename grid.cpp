#include "grid.h"
//ABCDEFGHIJ
//0123456789

void Grid::set_cell(const std::pair<char, int>& coord, const int data){
	cell[coord.first - 'A'][coord.second] = data;
}


int Grid::get_cell(const std::pair<char, int>& coord) const{
	return cell[coord.first - 'A'][coord.second];
}

std::ostream& operator<< (std::ostream& out, const Grid& grr){
	int c,i;
	for(c = 0; c <= 9; c++){
		for(i = 0; i <= 9; i++){
			out << grr.cell[c][i];
		}
		out << std::endl;
	}
	return out;
}
