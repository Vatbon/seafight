#ifndef PLAYER_H
#define PLAYER_H
//Arrange! Shoot! Miss Hit Kill Win! Lose Enemy shooted into G 4

#include <vector>
#include <utility>
#include <cstdlib>
#include "grid.h"

#define EMPTY 0
#define SHIP 1
#define SHOT 2
#define MISS 3

#define CMD_SHOOT 0
#define CMD_HIT 1
#define CMD_KILL 2

class Player{
	protected:
		std::pair<char, int> last_shot;
		Grid mygrid;
		Grid enemygrid;
		std::vector<std::pair<char, int>> plan;
		std::vector<std::pair<char, int>> claim;
	public:
		Grid arrange();
		std::pair<char, int> shot(const int);
};

bool is_free(const std::pair<char, int>& ,const Grid&);

#endif // PLAYER_H
