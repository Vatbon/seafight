#ifndef GRID_H
#define GRID_H

#include <utility>
#include <iostream>

class Grid{
	protected:
		int cell[10][10];
	public:
		void set_cell(const std::pair<char, int>&, const int);
		int get_cell(const std::pair<char, int>&) const;
		friend std::ostream& operator<<(std::ostream&, const Grid&);
};

#endif //GRID_H
