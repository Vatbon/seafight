#include <iostream>
#include <utility>
#include <string>
#include <time.h>
#include "grid.h"
#include "player.h"

void Start_Game(Player& player1){
	//read and transfer all into std::cin and std::cout	
	std::string command;
	std::pair<char, int> coord;
	std::string tmp;
	srand(time(NULL));
	while (1){
		std::getline(std::cin, command);
		if (command[0] == 'A'){
			std::cout << player1.arrange();
		}
		/*if (command[0] == 'M'){
			//What a pity, you've missed. 
		}*/
		if (command[0] == 'S'){
			coord = player1.shot(CMD_SHOOT);
			std::cout << coord.first << " " << coord.second << std::endl;
		}
		if (command[0] == 'H'){
			coord = player1.shot(CMD_HIT);
			std::cout << coord.first << " " << coord.second << std::endl;
		}
		if (command[0] == 'K'){
			coord = player1.shot(CMD_KILL);
			std::cout << coord.first << " " << coord.second << std::endl;
		}
		/*if (command[0] == 'E'){
			//Enemy shooted into somewhere.
		}*/
		if (command[0] == 'W')
			break;
		if (command[0] == 'L')
			break;
	}
}

int main(){
	Player player1;
	Start_Game(player1);
	return 0;
}
