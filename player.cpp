#include "player.h"

bool is_free(const std::pair<char, int>& coord,const Grid& grid_given ){
	char cor1 = coord.first;
	int cor2 = coord.second;
	bool res = true;
	char c;
	int i;
	if ( !(('A' <= cor1) && (cor1 <= 'J') && (0 <= cor2) && (cor2 <= 9)) ){
		res = false;
	}
	else
		for (c = cor1 - 1; c <= cor1 + 1; c++)
			for(i = cor2 - 1; i <= cor2 + 1; i++){
				if ( ('A' <= c) && (c <= 'J') && (0 <= i) && (i <= 9) )
					if (grid_given.get_cell(std::make_pair(c, i)) != EMPTY){
						res = false;
					}
			}
	return res;
}

Grid Player::arrange(){
	//#### - 1
	//### - 2
	//## - 3
	//# - 4
	int i,j;
	for(i = 0; i <= 9; i++)
		for(j = 0; j <= 9; j++){
			mygrid.set_cell(std::make_pair((char)(i+'A'), j), EMPTY);
			enemygrid.set_cell(std::make_pair((char)(i+'A'), j), EMPTY);
		}
	char cor1;
	int cor2;
	char dir;
	int ship_count, ship_size;
	bool fit = false;
	for (ship_size = 4, ship_count = 1; ship_size > 0; ship_size--, ship_count++){
		for(j = 0; j < ship_count; j++, fit = false){
			while(!fit){
				fit = true;
				cor1 = (unsigned char)rand() % 10 + 'A';
				cor2 = (unsigned int)rand() % 10;
				dir = rand() % 4;
				if (dir == 0)
					for (i = 0; i < ship_size; i++)
						fit = fit && is_free(std::make_pair(cor1, cor2 + i), mygrid);
				if (dir == 1)
					for (i = 0; i < ship_size; i++)
						fit = fit && is_free(std::make_pair(cor1 + i, cor2), mygrid);
				if (dir == 2)
					for (i = 0; i < ship_size; i++)
						fit = fit && is_free(std::make_pair(cor1, cor2 - i), mygrid);
				if (dir == 3)
					for (i = 0; i < ship_size; i++)
						fit = fit && is_free(std::make_pair(cor1 - i, cor2), mygrid);
			}
			if (dir == 0)
				for (i = 0; i < ship_size; i++)
					mygrid.set_cell(std::make_pair(cor1, cor2 + i), SHIP);
			if (dir == 1)
				for (i = 0; i < ship_size; i++)
					mygrid.set_cell(std::make_pair(cor1 + i, cor2), SHIP);
			if (dir == 2)
				for (i = 0; i < ship_size; i++)
					mygrid.set_cell(std::make_pair(cor1, cor2 - i), SHIP);
			if (dir == 3)
				for (i = 0; i < ship_size; i++)
					mygrid.set_cell(std::make_pair(cor1 - i, cor2), SHIP);
		}
	}
	return mygrid;
}

std::pair<char, int> Player::shot(const int type){
	char cor1;
	int cor2;
	if (type == CMD_SHOOT){
		cor1 = (unsigned char)rand() % 10 + 'A';
		cor2 = (unsigned int)rand() % 10;
		while (enemygrid.get_cell(std::make_pair(cor1, cor2)) == SHOT){
			cor1 = (unsigned char)rand() % 10 + 'A';
			cor2 = (unsigned int)rand() % 10;
		}
	}
	if (type == CMD_HIT){
		cor1 = last_shot.first;
		cor2 = last_shot.second;
		if ( (('A' <= cor1) && (cor1 <= ('J' - 1)) && (0 <= cor2) && (cor2 <= (9 - 1))) )
			enemygrid.set_cell(std::make_pair(cor1 + 1, cor2 + 1), SHOT);
		if ( ((('A' + 1) <= cor1) && (cor1 <= 'J') && ((0 + 1) <= cor2) && (cor2 <= 9)) )
			enemygrid.set_cell(std::make_pair(cor1 - 1, cor2 - 1), SHOT);
		if ( ((('A' + 1) <= cor1) && (cor1 <= 'J') && (0 <= cor2) && (cor2 <= (9 - 1))) )
			enemygrid.set_cell(std::make_pair(cor1 - 1, cor2 + 1), SHOT);
		if ( (('A' <= cor1) && (cor1 <= ('J' - 1)) && ((0 + 1) <= cor2) && (cor2 <= (9))) )
			enemygrid.set_cell(std::make_pair(cor1 + 1, cor2 - 1), SHOT);
		cor1 = (unsigned char)rand() % 10 + 'A';
		cor2 = (unsigned int)rand() % 10;
		while (enemygrid.get_cell(std::make_pair(cor1, cor2)) == SHOT){
			cor1 = (unsigned char)rand() % 10 + 'A';
			cor2 = (unsigned int)rand() % 10;
		}
	}
	if (type == CMD_KILL){
		/*for(auto& cord : claim){
			for (c = cord.first - 1; c <= cord.first +1; c++)
				for(i = cord.second - 1; i <= cord.second + 1; i++)
					enemygrid.set_cell(std::make_pair(c, i), SHOT);
		}*/
		cor1 = last_shot.first;
		cor2 = last_shot.second;
		if ( (('A' <= cor1) && (cor1 <= ('J' - 1)) && (0 <= cor2) && (cor2 <= (9 - 1))) )
			enemygrid.set_cell(std::make_pair(cor1 + 1, cor2 + 1), SHOT);
		if ( ((('A' + 1) <= cor1) && (cor1 <= 'J') && ((0 + 1) <= cor2) && (cor2 <= 9)) )
			enemygrid.set_cell(std::make_pair(cor1 - 1, cor2 - 1), SHOT);
		if ( ((('A' + 1) <= cor1) && (cor1 <= 'J') && (0 <= cor2) && (cor2 <= (9 - 1))) )
			enemygrid.set_cell(std::make_pair(cor1 - 1, cor2 + 1), SHOT);
		if ( (('A' <= cor1) && (cor1 <= ('J' - 1)) && ((0 + 1) <= cor2) && (cor2 <= (9))) )
			enemygrid.set_cell(std::make_pair(cor1 + 1, cor2 - 1), SHOT);
		cor1 = (unsigned char)rand() % 10 + 'A';
		cor2 = (unsigned int)rand() % 10;
		while (enemygrid.get_cell(std::make_pair(cor1, cor2)) == SHOT){
			cor1 = (unsigned char)rand() % 10 + 'A';
			cor2 = (unsigned int)rand() % 10;
		}
	}
	last_shot = std::make_pair(cor1, cor2);
	enemygrid.set_cell(std::make_pair(cor1, cor2), SHOT);
	return std::make_pair(cor1, cor2);
}
